# The container is based on ubuntu 18.10
FROM ubuntu:18.10

WORKDIR /var/www/html

# Install apache, php and some extensions via apt
RUN apt-get update && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-get install -yq \
    apache2 \
    libapache2-mod-php7.2 \
    composer \
    php-pgsql && \
    a2enmod rewrite

ARG environment=example

# Move configuration files to where they belong
COPY config/$environment/000-default.conf /etc/apache2/sites-available/000-default.conf

# Make the container start the apache server on startup
ENTRYPOINT ["apachectl", "-DFOREGROUND"]

# Make the destination directory empty
RUN rm ./*

COPY webroot/composer.json ./

# Make composer install the dependencies
RUN composer install --no-plugins --no-scripts

COPY config/$environment/config.php ./../config/

COPY webroot ./
