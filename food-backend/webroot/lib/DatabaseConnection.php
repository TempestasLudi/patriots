<?php

namespace lib;

use PDO;

class DatabaseConnection
{
    private $db;

    public function __construct($config)
    {
        $this->db = new PDO("pgsql:host=$config->host;port=$config->port;dbname=$config->database", $config->user, $config->password);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function fetchObject($query, array $params = []): array
    {
        return $this->fetch($query, function ($line) {
            return $line;
        }, $params);
    }

    public function fetch(string $query, callable $callback, array $params = []): array
    {
        $output_array = [];

        $statement = $this->db->prepare($query);

        $statement->execute($params);

        $json_columns = [];
        for ($i = 0; $i < $statement->columnCount(); $i++) {
            $meta = (object)$statement->getColumnMeta($i);
            if ($meta->native_type == "json")
                $json_columns[] = $meta->name;
        }

        while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $line = (object)$callback($row);
            foreach ($json_columns as $column)
                $line->$column = json_decode($line->$column);
            $output_array[] = $line;
        }

        $statement->closeCursor();

        return $output_array;
    }

    public function execute(string $query, array $params)
    {
        return $this->db->prepare($query)->execute($params);
    }
}