<?php

use lib\Model;

require_once $_SERVER["DOCUMENT_ROOT"] . "/bootstrap.php";

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Allow-Headers: Content-Type");

$model = new Model($config->postgres);
switch ($_SERVER["REQUEST_METHOD"]) {
    case "PUT":
        $recipe = json_decode(file_get_contents('php://input'));
        $id = $model->saveRecipe($recipe);
        echo json_encode($model->fetch($id));
        break;
}
