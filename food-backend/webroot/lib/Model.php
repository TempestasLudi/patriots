<?php

namespace lib;

class Model
{
    private $database;

    private $select_recipe_query = <<<SQL
SELECT recipe.id,
       recipe.name,
       COALESCE(tag_list.tags, '[]') AS tags,
       COALESCE(ingredient_list.ingredients, '[]') AS ingredients
FROM recipe
LEFT JOIN (
    SELECT recipe_id,
           json_agg(tag_name) AS tags
    FROM recipe_tag
    GROUP BY recipe_id
) AS tag_list
    ON tag_list.recipe_id = recipe.id
LEFT JOIN (
    SELECT recipe_id,
           json_agg(json_build_object('name', name, 'amount', amount, 'unit', unit)) AS ingredients
    FROM ingredient
    GROUP BY recipe_id
) AS ingredient_list
    ON ingredient_list.recipe_id = recipe.id
SQL;

    private $insert_recipe_query = <<<SQL
INSERT INTO recipe (name)
            VALUES (:name)
    RETURNING id;
SQL;

    private $update_recipe_query = <<<SQL
UPDATE recipe
    SET name=:name
    WHERE id=:id;
SQL;

    private $delete_tags_query = <<<SQL
DELETE
    FROM recipe_tag
    WHERE recipe_id = :id;
SQL;

    private $insert_tags_query = <<<SQL
INSERT INTO recipe_tag
    SELECT *
    FROM json_populate_recordset(null::recipe_tag, :json);
SQL;

    private $delete_ingredients_query = <<<SQL
DELETE
    FROM ingredient
    WHERE recipe_id = :id;
SQL;

    private $insert_ingredients_query = <<<SQL
INSERT INTO ingredient
    SELECT *
    FROM json_populate_recordset(null::ingredient, :json);
SQL;

    public function __construct($config)
    {
        $this->database = new DatabaseConnection($config);
    }

    public function fetchAll()
    {
        return $this->database->fetchObject($this->select_recipe_query . " ORDER BY recipe.name ASC;");
    }

    public function fetch($id)
    {
        return $this->database->fetchObject(
            $this->select_recipe_query . " WHERE recipe.id = :id;",
            ["id" => $id]
        )[0];
    }

    public function saveRecipe($recipe)
    {
        $id = null;
        if (isset($recipe->id)) {
            $id = $recipe->id;
            $this->database->execute($this->update_recipe_query, [
                "id" => $id,
                "name" => $recipe->name
            ]);
        } else {
            $id = $this->database->fetchObject($this->insert_recipe_query, [
                "name" => $recipe->name
            ])[0]->id;
        }

        $this->database->execute($this->delete_tags_query, [
            "id" => $id,
        ]);

        $this->database->execute($this->insert_tags_query, [
            "json" => json_encode(
                array_map(
                    function ($tag) use ($id) {
                        return [
                            "recipe_id" => $id,
                            "tag_name" => $tag
                        ];
                    },
                    $recipe->tags
                )
            )
        ]);

        $this->database->execute($this->delete_ingredients_query, [
            "id" => $id
        ]);

        $this->database->execute($this->insert_ingredients_query, [
            "json" => json_encode(
                array_map(
                    function ($index, $ingredient) use ($id) {
                        return [
                            "recipe_id" => $id,
                            "index" => $index,
                            "name" => $ingredient->name,
                            "amount" => $ingredient->amount,
                            "unit" => $ingredient->unit
                        ];
                    },
                    array_keys($recipe->ingredients),
                    $recipe->ingredients
                )
            )
        ]);

        return $id;
    }
}
