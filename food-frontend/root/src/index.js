import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import RecipeList from "./components/RecipeList";

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            recipeDialog: null,
            filter: ""
        };
    }

    render() {
        return (<div className="Main">
            <header>
                <h1>Gerechten</h1>
            </header>
            <main className="Content">
                <input className="filter"
                       placeholder="Zoeken"
                       type="text"
                       value={this.state.filter}
                       onChange={e => this.setState({filter: e.target.value.toLowerCase()})}/>
                <RecipeList filter={this.state.filter}/>
            </main>
        </div>);
    }
}

ReactDOM.render(<Main/>, document.getElementById('root'));
