import React from 'react';
import RecipeDialog from "./RecipeDialog";
import "./css/RecipeEntry.css";
import "./css/tag.css";
import AddIngredientsDialog from "./AddIngredientsDialog";

export default class RecipeEntry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            recipeDialog: null,
            addIngredientsDialog: null,
            data: props.recipe
        }
    }

    containsText(text) {
        return (this.state.data.name || "").toLowerCase().indexOf(text) > -1 ||
            (this.state.data.tags.length > 0
                && this.state.data.tags.map(t => t.toLowerCase().indexOf(text) > -1)
                    .reduce((a, b) => a || b)
            );
    }

    saveRecipe(recipe) {
        fetch(`${window.config.api.baseUrl}/recipe.php`, {
            method: "PUT",
            body: JSON.stringify(recipe),
            headers: {
                "Content-Type": "application/json"
            }
        }).then(data => data.json())
            .then(data => {
                    this.setState(state => {
                        state.recipeDialog = null;
                        state.data = data;
                        return state;
                    });
                    this.props.onChange(data);
                }
            );
    }

    render() {
        if (
            !this.containsText(this.props.textFilter) ||
            (this.props.tagFilter != null && this.state.data.tags.indexOf(this.props.tagFilter) === -1)
        ) return null;

        return <div className="RecipeEntry" onClick={() => this.setState({
            recipeDialog:
                <RecipeDialog recipe={this.state.data}
                              onSave={this.saveRecipe.bind(this)}
                              onClose={() => this.setState({recipeDialog: null})}/>
        })}>
            <span className="RecipeEntry/name" title={this.state.data.name}>{this.state.data.name}</span>
            <span className="RecipeEntry/buttons">
                <span className="RecipeEntry/buttons/button material-icons" onClick={e => {
                    this.setState({
                        addIngredientsDialog:
                            <AddIngredientsDialog name={this.state.data.name}
                                                  ingredients={this.state.data.ingredients}
                                                  onSave={ingredients => {
                                                      this.setState({addIngredientsDialog: null});
                                                      (this.props.onAddToGroceryList || (() => {}))(ingredients);
                                                  }}
                                                  onClose={() => this.setState({addIngredientsDialog: null})}/>
                    });
                    e.stopPropagation();
                }}>add_shopping_cart</span>
            </span>
            <span className="RecipeEntry/tags">
                {this.state.data.tags.map((t, i) => <span className="RecipeEntry/tags/tag tag" key={i}>{t}</span>)}
            </span>
            {this.state.recipeDialog}
            {this.state.addIngredientsDialog}
        </div>;
    }
}
