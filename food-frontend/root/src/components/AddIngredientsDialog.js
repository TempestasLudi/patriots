import React from 'react';
import "./css/AddIngredientsDialog.css";

export default class AddIngredientsDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ingredients: props.ingredients.map(ingredient => ({
                enabled: true,
                amount: ingredient.amount
            }))
        };
    }

    setIngredient(ingredient, index) {
        this.setState(state => {
            let newState = {ingredients: [...state.ingredients]};
            Object.assign(newState.ingredients[index], ingredient);
            return newState;
        });
    }

    render() {
        return <div className="AddIngredientsDialog" onClick={e => e.stopPropagation()}>
            <div className="AddIngredientsDialog/overlay" onClick={() => (this.props.onClose || (() => {}))()}/>
            <div className="AddIngredientsDialog/content">
                <span className="AddIngredientsDialog/title">Voeg {this.props.name} toe aan boodschappenlijst</span>
                <ul className="AddIngredientsDialog/ingredients">
                    {this.state.ingredients.map((ingredient, index) =>
                        <li key={index} className="AddIngredientsDialog/ingredients/ingredient">
                            <input type="checkbox" checked={ingredient.enabled}
                                   onChange={e => this.setIngredient({enabled: e.target.checked}, index)}/>
                            <input type="number" disabled={!ingredient.enabled} value={ingredient.amount}
                                   onChange={e => this.setIngredient({amount: e.target.value}, index)}/>
                            <span>{this.props.ingredients[index].unit}</span>
                            <span>{this.props.ingredients[index].name}</span>
                        </li>
                    )}
                </ul>
                <button onClick={() => (this.props.onSave || (() => {}))(
                    this.state.ingredients.map((ingredient, index) => ({ingredient: ingredient, index: index}))
                        .filter(i => i.ingredient.enabled)
                        .map(i => ({
                            ...this.props.ingredients[i.index],
                            amount: i.ingredient.amount
                        })))}>
                    Toevoegen
                </button>
                <button onClick={() => (this.props.onClose || (() => {}))()}>Annuleren</button>
            </div>
        </div>
    }
}
