import React from 'react';
import RecipeEntry from "./RecipeEntry";
import RecipeDialog from "./RecipeDialog";
import "./css/RecipeEntry.css";
import "./css/RecipeList.css";
import "./css/tag.css";

export default class RecipeList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            recipes: [],
            tags: [],
            selectedTag: null,
            recipeDialog: null
        };
    }

    componentDidMount() {
        fetch(`${window.config.api.baseUrl}/recipes.php`)
            .then(data => data.json())
            .then(data => this.setState({recipes: data, tags: RecipeList.getTags(data)}));
    }

    openRecipeDialog() {
        this.setState({
            recipeDialog: <RecipeDialog onSave={this.addRecipe.bind(this)}
                                        onClose={() => this.setState({recipeDialog: null})}/>
        });
    }

    addRecipe(recipe) {
        fetch(`${window.config.api.baseUrl}/recipes.php`, {
            method: "POST",
            body: JSON.stringify(recipe),
            headers: {
                "Content-Type": "application/json"
            }
        }).then(data => data.json())
            .then(data => this.setState(state => ({
                recipeDialog: null,
                recipes: [...state.recipes, data],
                tags: RecipeList.getTags([...state.recipes, data])
            })));
    }

    changeRecipe(i, recipe) {
        this.setState(state => {
            let newRecipes = [...state.recipes];
            newRecipes[i] = recipe;
            let newTags = RecipeList.getTags(newRecipes);
            return {
                recipes: newRecipes,
                tags: newTags,
                selectedTag: newTags.indexOf(state.selectedTag) < 0 ? null : state.selectedTag
            };
        });
    }

    static getTags(recipes) {
        return recipes.flatMap(r => r.tags)
            .sort()
            .reduce((list, tag) => list.length > 0 && list[list.length - 1] === tag ? list : [...list, tag], []);
    }

    render() {
        return (<div className="RecipeList">
            <div className="RecipeList/tags">
                {
                    this.state.tags.map(tag =>
                        <span key={tag}
                              onClick={() => this.setState(state => ({selectedTag: state.selectedTag === tag ? null : tag}))}
                              className={`RecipeList/tags/tag tag${tag === this.state.selectedTag ? " selected" : ""}`}>
                        {tag}
                        </span>)
                }
            </div>
            <div className="RecipeEntry RecipeList/newRecipe"
                 onClick={this.openRecipeDialog.bind(this)}>
                <span className="RecipeEntry/name">Nieuw gerecht</span>
            </div>
            <div className="RecipeList/list">
                {this.state.recipes.map((recipe, i) =>
                    <RecipeEntry key={recipe.id}
                                 recipe={recipe}
                                 textFilter={this.props.filter}
                                 tagFilter={this.state.selectedTag}
                                 onChange={newRecipe => this.changeRecipe(i, newRecipe)}
                                 onAddToGroceryList={ingredients => console.log(recipe.name, ingredients)}/>
                )}
            </div>
            {this.state.recipeDialog}
        </div>);
    }
}