import React from 'react';
import "./css/RecipeDialog.css";
import "./css/tag.css";
import 'material-design-icons/iconfont/material-icons.css';

export default class RecipeDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            onSave: props.onSave || (() => {
            }),
            onClose: props.onClose || (() => {
            }),
            data: props.recipe || {
                name: "",
                tags: [],
                ingredients: []
            }
        };
    }

    save() {
        this.addTag(this.refs.tagInput.value.trim(), () => this.props.onSave(this.state.data));
    }

    tagInputKeyPress() {
        let input = this.refs.tagInput;
        let text = input.value;
        if (text.indexOf(",") >= 0) {
            let textBeforeComma = text.split(",")[0];
            this.addTag(textBeforeComma);
            input.value = text.substring(textBeforeComma.length + 1).trim();
        }
    }

    addTag(tag, callback = (() => {})) {
        tag = tag.trim().toLowerCase();
        if (tag.length === 0) return callback();
        if (this.state.data.tags.indexOf(tag) >= 0) return callback();
        let newTags = [...this.state.data.tags, tag];
        newTags.sort();
        this.setState(state => ({data: {...state.data, tags: newTags}}), callback);
    }

    removeTag(t) {
        this.setState(state => {
            let newTags = [...state.data.tags];
            let index = newTags.indexOf(t);
            console.log(newTags, t, index);
            if (index < 0) return {};
            newTags.splice(index, 1);
            return {data: {...state.data, tags: newTags}};
        });
    }

    setIngredientProperty(index, property, value) {
        let newIngredients = [...this.state.data.ingredients];
        if (this.state.data.ingredients[index] === undefined) {
            if (property.length === 0) return;
            newIngredients = [...newIngredients, {
                name: "",
                amount: 0,
                unit: ""
            }];
        }
        newIngredients[index][property] = value;
        let ingredient = newIngredients[index];
        if (ingredient.name === "" && ingredient.amount === 0 && ingredient.unit === "") {
            newIngredients.splice(index, 1);
        }
        this.setState(state => ({data: {...state.data, ingredients: newIngredients}}));
    }

    render() {
        return <div className="RecipeDialog" onClick={e => e.stopPropagation()}>
            <div className="RecipeDialog/overlay" onClick={this.props.onClose}></div>
            <div className="RecipeDialog/content">
                <input className="RecipeDialog/name"
                       onChange={e => this.setState(({data: {...this.state.data, name: e.target.value}}))}
                       value={this.state.data.name}/>
                <h2>Tags</h2>
                <div className="RecipeDialog/tags">
                    {this.state.data.tags.map(t => <span key={t} className="RecipeDialog/tags/tag tag">
                        {t}
                        <i className="material-icons RecipeDialog/tags/tag/delete" onClick={() => this.removeTag(t)}>clear</i>
                    </span>)}

                    <input onKeyPress={() => setTimeout(this.tagInputKeyPress.bind(this), 1)}
                           className="RecipeDialog/tags/tagInput"
                           ref="tagInput"/>
                </div>
                <h2>Ingrediënten</h2>
                <ul className="RecipeDialog/ingredients">
                    {this.state.data.ingredients.concat({name: "", amount: 0, unit: ""}).map((o, i) => <li key={i} className="RecipeDialog/ingredients/ingredient">
                        <input type="number" value={o.amount} placeholder="hoeveelheid" className="RecipeDialog/ingredients/ingredient/amount"
                               onChange={e => this.setIngredientProperty(i, "amount", parseFloat(e.target.value) || 0)}/>
                        <input type="text" value={o.unit} placeholder="eenheid" className="RecipeDialog/ingredients/ingredient/unit"
                               onChange={e => this.setIngredientProperty(i, "unit", e.target.value.trim())}/>
                        <input type="text" value={o.name} placeholder="naam" className="RecipeDialog/ingredients/ingredient/name"
                               onChange={e => this.setIngredientProperty(i, "name", e.target.value.trim())}/>
                    </li>)}
                </ul>
                <button onClick={this.save.bind(this)}>Opslaan</button>
                <button onClick={() => this.state.onClose()}>Annuleren</button>
            </div>
        </div>;
    }
}
